import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { PetModel } from '../pet.model';

@Injectable()
export class HttpService {
  constructor(private httpClient: HttpClient) {}

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });

  headersImage = new HttpHeaders({
    'Content-Type': 'multipart/form-data',
    Accept: 'application/json',
  });

  createUser(): Observable<any> {
    const url = `http://localhost/v2/user`;
    return this.httpClient.post(
      url,
      {
        id: 0,
        username: 'test',
        firstName: 'test',
        lastName: 'test',
        email: 'test@gmail.com',
        password: '123456',
        phone: '782733',
        userStatus: 0,
      },
      { headers: this.headers }
    );
  }

  login(loginForm: FormGroup): Observable<any> {
    const url = `http://localhost/v2/user/login?username=${loginForm.controls['username'].value}&password=${loginForm.controls['password'].value}`;
    return this.httpClient.get(url, { headers: this.headers });
  }

  creatPet(payload: FormGroup): Observable<PetModel> {
    const url = `http://localhost/v2/pet`;
    return this.httpClient.post<PetModel>(
      url,
      {
        name: payload.controls['name'].value,
        status: payload.controls['status'].value,
      },
      { headers: this.headers }
    );
  }

  findPetByStatus(): Observable<PetModel[]> {
    const url = `http://localhost/v2/pet/findByStatus?status=available`;
    return this.httpClient.get<PetModel[]>(url, { headers: this.headers });
  }

  uploadImage(id: number, formData: FormData): Observable<any> {
    const url = `http://localhost/v2/pet/${id}/uploadImage`;
    return this.httpClient.post(url, formData);
  }

  getPetById(id: number): Observable<PetModel> {
    console.log(id);
    const url = `http://localhost/v2/pet/${id}`;
    console.log(url);
    return this.httpClient.get<PetModel>(url, {
      headers: { Accept: 'application/json' },
    });
  }
}
