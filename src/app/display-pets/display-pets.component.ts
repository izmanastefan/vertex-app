import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ModalPetComponent } from '../modal-pet/modal-pet.component';
import { PetModel } from '../pet.model';
import { HttpService } from '../service/http-service';

@Component({
  selector: 'app-display-pets',
  templateUrl: './display-pets.component.html',
  styleUrls: ['./display-pets.component.scss'],
})
export class DisplayPetsComponent implements OnInit {
  products: PetModel[] = [];

  constructor(private httpService: HttpService, public dialog: MatDialog) {}

  ngOnInit() {
    this.httpService.findPetByStatus().subscribe((data) => {
      this.products = data;
    });
  }

  openDialog(id: number): void {
    this.dialog.open(ModalPetComponent, { data: { id: id } });
  }
}
