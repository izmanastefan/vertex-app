import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PetModel } from '../pet.model';
import { HttpService } from '../service/http-service';

@Component({
  selector: 'app-create-pet',
  templateUrl: './create-pet.component.html',
  styleUrls: ['./create-pet.component.scss'],
})
export class CreatePetComponent implements OnInit {
  petData: PetModel | undefined;

  reader = new FileReader();

  constructor(private httpService: HttpService, private router: Router) {}

  ngOnInit(): void {}

  petForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  });

  onUploadImage(event: any) {
    const formData = new FormData();
    if (event.files && this.petData?.id) {
      formData.append('file', event.files);
      console.log(formData.getAll)
      this.httpService.uploadImage(this.petData?.id, formData).subscribe();
    }
  }

  onCreatePet() {
    if (this.petForm.valid) {
      this.httpService.creatPet(this.petForm).subscribe((data) => {
        this.petData = data;
      });
    }
  }
}
