import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PetModel } from '../pet.model';
import { HttpService } from '../service/http-service';

@Component({
  selector: 'app-modal-pet',
  templateUrl: './modal-pet.component.html',
  styleUrls: ['./modal-pet.component.scss'],
})
export class ModalPetComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    public dialogRef: MatDialogRef<ModalPetComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  petData: PetModel | undefined;

  ngOnInit(): void {
    console.log(this.data.id)
    if (this.data.id) {
      this.httpService.getPetById(this.data.id).subscribe((data) => {
        this.petData = data;
      });
    }
  }
}
