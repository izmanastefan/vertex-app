import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePetComponent } from './create-pet/create-pet.component';
import { DisplayPetsComponent } from './display-pets/display-pets.component';
import { LoginComponent } from './login/login.component';
import { PreloaderComponent } from './preloader/preloader.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'create', component: CreatePetComponent },
  { path: 'details', component: DisplayPetsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
