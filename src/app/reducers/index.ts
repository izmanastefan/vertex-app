import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { PetAction, PetActionTypes } from '../actions/pet.actions';

import { PetModel } from '../pet.model';

export interface PetState {
  pet: PetModel | undefined;
}

const initialPetState: PetState = {
  pet: {
    id: 0,
    category: {
      id: 0,
      name: '',
    },
    name: '',
    photoUrls: [],
    tags: [
      {
        id: 0,
        name: '',
      },
    ],
    status: '',
  },
};

export interface AppState {
  pet: PetState | undefined;
}

export function petReducer(
  state: PetState = initialPetState,
  action: PetAction
): PetState {
  switch (action.type) {
    case PetActionTypes.LoadPet:
      return {
        pet: action.payload?.petData,
      };

    default:
      return state;
  }
}

export const selectPet = (state: AppState) => state.pet;

export const selectError = (state: AppState) => state.pet;


export const metaReducers: MetaReducer<AppState>[] = !environment.production
  ? []
  : [];
