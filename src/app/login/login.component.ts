import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../service/http-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private httpService: HttpService, private router: Router) {}

  ngOnInit(): void {}

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  submit() {
    if (this.loginForm.valid) {
      this.httpService.login(this.loginForm).subscribe((data) => {
        if (data) {
          this.router.navigate(['/details'], { queryParams: { id: 'new' } });
        }
      });
    }
  }
  @Input() error: string | null = '';
}
