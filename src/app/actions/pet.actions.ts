import { Action } from '@ngrx/store';
import { PetModel } from '../pet.model';


export enum PetActionTypes {
  LoadPet = '[Home Page] Load Pets',
  PetError = '[Home Page] Pets Error'
}

export class PetAction implements Action {
  type!: string;
  payload: {
   petData: PetModel;
    error: string;
  } | undefined;
}

export class LoadPets implements Action {
  readonly type = PetActionTypes.LoadPet;

  constructor(readonly payload: {petData: PetModel}) {
 
  }
}

export class PetsError implements Action {
  readonly type = PetActionTypes.PetError;

  constructor(readonly payload: {error: string}) {

  }
}


export type ActionsUnion = LoadPets | PetsError;